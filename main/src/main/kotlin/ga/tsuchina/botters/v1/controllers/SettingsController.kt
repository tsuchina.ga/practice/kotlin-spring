package ga.tsuchina.botters.v1.controllers

import ga.tsuchina.botters.v1.services.SettingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class SettingsController {

    @Autowired
    lateinit var service: SettingService

    @GetMapping("/v1/settings")
    @ResponseStatus(HttpStatus.OK)
    fun get(): Map<String, Any?> {
        return mapOf("settings" to service.findAll())
    }
}
