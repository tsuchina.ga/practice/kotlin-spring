package ga.tsuchina.botters.v1.aspects

import ga.tsuchina.botters.v1.exceptions.DuplicateRecordException
import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.exceptions.NotValidException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.text.SimpleDateFormat
import java.util.Date
import javax.servlet.http.HttpServletRequest

@RestControllerAdvice(basePackages = ["ga.tsuchina.botters.v1"])
class ExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(NotValidException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun notValidException(e: NotValidException, request: HttpServletRequest) = mapOf(
            "timestmap" to SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(Date()),
            "status" to 400,
            "error" to "BAD REQUEST",
            "message" to e.message,
            "path" to request.requestURI
    )

    @ExceptionHandler(NotFoundRecordException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun notFoundRecordException(e: NotFoundRecordException, request: HttpServletRequest) = mapOf(
            "timestmap" to SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(Date()),
            "status" to 404,
            "error" to "NOT FOUND",
            "message" to e.message,
            "path" to request.requestURI
    )

    @ExceptionHandler(DuplicateRecordException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun duplicateRecordException(e: DuplicateRecordException, request: HttpServletRequest) = mapOf(
            "timestmap" to SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(Date()),
            "status" to 400,
            "error" to "BAD REQUEST",
            "message" to e.message,
            "path" to request.requestURI
    )
}
