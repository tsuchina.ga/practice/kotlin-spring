package ga.tsuchina.botters.v1.aspects

import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import java.text.SimpleDateFormat
import java.util.Date

@Aspect
@Component
class ControllerAspect {

    @Around("within(ga.tsuchina.botters.v1.controllers.*)")
    fun around(point: ProceedingJoinPoint): Any? {
//        println("処理開始: " + System.currentTimeMillis())

        val request = (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request

        val result = point.proceed()

        val response = (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes).response

        val path = request.requestURI
        val code = response?.status

//        println("処理終了: " + System.currentTimeMillis())

        return mapOf(
                "timestmap" to SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").format(Date()),
                "status" to code,
                "message" to "success",
                "path" to path,
                "result" to result
        )
    }
}
