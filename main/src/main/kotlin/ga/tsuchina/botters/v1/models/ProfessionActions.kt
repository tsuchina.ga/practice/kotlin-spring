package ga.tsuchina.botters.v1.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.Min

@Entity
@Table(name = "profession_actions")
data class ProfessionActions (

    @field: Id
    @field: GeneratedValue(strategy = GenerationType.IDENTITY)
    @field: Column(name = "id")
    var id: Int = -1,

    @field: Column(name = "profession_id")
    @field: Min(1)
    var professionId: Int = 0,

    @field: Column(name = "action_id")
    @field: Min(1)
    var actionId: Int = 0,

    @field: Column(name = "cost")
    @field: Min(0)
    var cost: Int = 0
)