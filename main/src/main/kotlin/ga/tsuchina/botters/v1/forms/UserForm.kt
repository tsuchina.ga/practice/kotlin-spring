package ga.tsuchina.botters.v1.forms

import java.io.Serializable
import javax.validation.constraints.Pattern

data class PostUserForm(

    @field: Pattern(regexp = "^[a-zA-Z0-9]{6,32}$", message = "loginidは大小英数字6～32文字で入力してください")
    val loginid: String = "",

    @field: Pattern(regexp = "^[a-zA-Z0-9]{6,32}$", message = "passwordは大小英数字6～32文字で入力してください")
    val password: String = "",

    @field: Pattern(regexp = "^[a-zA-Z0-9]{4,32}$", message = "nameは大小英数字4～16文字で入力してください")
    val name: String = ""
) : Serializable

data class PutUserIdForm(

    @field: Pattern(regexp = "^[a-zA-Z0-9]{6,32}$", message = "passwordは大小英数字6～32文字で入力してください")
    val password: String? = null,

    @field: Pattern(regexp = "^[a-zA-Z0-9]{4,32}$", message = "nameは大小英数字4～16文字で入力してください")
    val name: String? = null
) : Serializable
