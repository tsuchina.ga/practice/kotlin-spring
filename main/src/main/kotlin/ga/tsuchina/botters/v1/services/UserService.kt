package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.configs.SHA256Hasher
import ga.tsuchina.botters.v1.exceptions.DuplicateRecordException
import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.forms.PostUserForm
import ga.tsuchina.botters.v1.forms.PutUserIdForm
import ga.tsuchina.botters.v1.models.User
import ga.tsuchina.botters.v1.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service
import java.util.Date

@Service
class UserService {

    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var shA256Hasher: SHA256Hasher

    fun findAll(): List<User> = repository.findAll(Sort(Sort.Direction.ASC, "id"))

    fun findByLoginid(loginid: String): User {

        val user = repository.findByLoginid(loginid)
        user?.let { return user }
        throw NotFoundRecordException()
    }

    fun create(form: PostUserForm): User {

        val user = repository.findByLoginid(form.loginid)
        if (user == null) {
            return repository.save(User(
                loginid = form.loginid,
                password = shA256Hasher.create(form.loginid, form.password),
                name = form.name
            ))
        } else throw DuplicateRecordException()
    }

    fun updateByLoginid(loginid: String, form: PutUserIdForm): User {

        val user = repository.findByLoginid(loginid)
        if (user != null) {
            return repository.save(user.copy(
                password =
                if (form.password != null) shA256Hasher.create(user.loginid, form.password)
                else user.password,
                name = form.name ?: user.name,
                updatedAt = Date()
            ))
        } else throw NotFoundRecordException()
    }

    fun deleteByLoginid(loginid: String): User {

        val user = repository.findByLoginid(loginid)
        if (user != null) return repository.save(user.copy(isDeleted = 1, updatedAt = Date()))
        else throw NotFoundRecordException()
    }
}
