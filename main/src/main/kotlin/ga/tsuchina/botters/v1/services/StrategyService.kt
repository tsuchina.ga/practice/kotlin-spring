package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Strategy
import ga.tsuchina.botters.v1.repositories.StrategyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class StrategyService {

    @Autowired
    lateinit var repository: StrategyRepository

    fun findById(id: Int): Strategy {
        val strategy = repository.findById(id).orElse(null)
        strategy?.let { return strategy }
        throw NotFoundRecordException()
    }
}