package ga.tsuchina.botters.v1.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Min

@Entity
@Table(name = "bot_professions")
@Where(clause = "is_deleted = 0")
data class BotProfessions (

    @field: Id
    @field: GeneratedValue(strategy = GenerationType.IDENTITY)
    @field: Column(name = "id")
    var id: Int = -1,

    @field: Column(name = "bot_id")
    @field: Min(1)
    var botId: Int = 0,

    @field: Column(name = "profession_id")
    @field: Min(1)
    var professionId: Int = 0,

    @field: Column(name = "level")
    var level: Int = 0,

    @field: Column(name = "is_deleted")
    var isDeleted: Int = 0,

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "created_at")
    var createdAt: Date = Date(),

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "updated_at")
    var updatedAt: Date = Date()
)