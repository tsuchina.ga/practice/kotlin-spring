package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.ProfessionActions
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ProfessionActionsRepository : JpaRepository<ProfessionActions, Int> {

    fun findByProfessionId(professionId: Int): List<ProfessionActions>
}
