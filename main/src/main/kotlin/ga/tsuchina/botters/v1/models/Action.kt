package ga.tsuchina.botters.v1.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "actions")
data class Action (

    @field: Id
    @field: GeneratedValue(strategy = GenerationType.IDENTITY)
    @field: Column(name = "id")
    var id: Int = -1,

    @field: Column(name = "name", unique = true)
    @field: NotNull
    @field: Size(min = 1, max = 16)
    var name: String = ""
)