package ga.tsuchina.botters.v1.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotEmpty

@Entity
@Table(name = "settings")
data class Setting (

    @field: Id
    @field: Column(name = "key", unique = true)
    @field: NotEmpty
    var key: String = "",

    @field: Column(name = "value")
    var value: String = ""
)
