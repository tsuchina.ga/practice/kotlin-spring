package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Strategy
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface StrategyRepository : JpaRepository<Strategy, Int> {

    @Transactional
    @Modifying
    @Query("DELETE FROM Strategy")
    fun deleteAllRecords()
}
