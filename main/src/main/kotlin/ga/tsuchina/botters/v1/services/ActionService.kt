package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Action
import ga.tsuchina.botters.v1.repositories.ActionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ActionService {

    @Autowired lateinit var repository: ActionRepository

    fun findById(id: Int): Action {
        val action = repository.findById(id).orElse(null)
        action?.let { return action }
        throw NotFoundRecordException()
    }
}