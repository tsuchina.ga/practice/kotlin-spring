package ga.tsuchina.botters.v1.exceptions

class NotValidException(message: String? = "Not Valid Datas", cause: Throwable? = null) :
    RuntimeException(message, cause)

class NotFoundRecordException(message: String? = "Not Found", cause: Throwable? = null) :
    RuntimeException(message, cause)

class DuplicateRecordException(message: String? = "Duplicate Recoeds", cause: Throwable? = null) :
    RuntimeException(message, cause)
