package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.BotProfessions
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface BotProfessionsRepository : JpaRepository<BotProfessions, Int> {

    fun findByBotId(botId: Int): List<BotProfessions>

    @Transactional
    @Modifying
    @Query("DELETE FROM BotProfessions")
    fun deleteAllRecords()
}