package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface UserRepository : JpaRepository<User, Int>, JpaSpecificationExecutor<User> {

    fun findByLoginid(loginid: String): User?

    @Transactional
    @Modifying
    @Query("DELETE FROM User")
    fun deleteAllRecords()
}
