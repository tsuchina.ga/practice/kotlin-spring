package ga.tsuchina.botters.v1.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

@Entity
@Table(name = "users")
@Where(clause = "is_deleted = 0")
data class User (

    @field: JsonIgnore
    @field: Id
    @field: GeneratedValue(strategy = GenerationType.IDENTITY)
    @field: Column(name = "id")
    var id: Int = -1,

    @field: Column(name = "loginid")
    @field: Size(min = 6, max = 32)
    @field: Pattern(regexp = "^[a-zA-Z0-9]+$")
    var loginid: String = "",

    @field: JsonIgnore
    @field: Column(name = "password")
    @field: Size(min = 6, max = 256)
    @field: Pattern(regexp = "^[a-zA-Z0-9]+$")
    var password: String = "",

    @field: Column(name = "name")
    @field: Size(min = 4, max = 32)
    @field: Pattern(regexp = "^[a-zA-Z0-9]+$")
    var name: String = "",

    @field: Column(name = "money")
    var money: Int = 0,

    @field: JsonIgnore
    @field: Column(name = "is_deleted")
    var isDeleted: Int = 0,

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "created_at")
    var createdAt: Date = Date(),

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "updated_at")
    var updatedAt: Date = Date()
)
