package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Setting
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SettingRepository : JpaRepository<Setting, String> {

    fun findByKey(key: String): Setting?
}
