package ga.tsuchina.botters.v1.controllers

import ga.tsuchina.botters.v1.exceptions.NotValidException
import ga.tsuchina.botters.v1.forms.PostUserForm
import ga.tsuchina.botters.v1.forms.PutUserIdForm
import ga.tsuchina.botters.v1.services.BotService
import ga.tsuchina.botters.v1.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.validation.Errors
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class UsersController {

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var botService: BotService

    @GetMapping("/v1/users")
    @ResponseStatus(HttpStatus.OK)
    fun getUser(): Map<String, Any?> {
        return mapOf("users" to userService.findAll())
    }

    @PostMapping("/v1/users")
    @ResponseStatus(HttpStatus.OK)
    fun postUser(@RequestBody @Validated form: PostUserForm, errors: Errors): Map<String, Any?> {
        if (errors.hasErrors()) {
            throw NotValidException(errors.allErrors.map { e -> e.defaultMessage }.toString())
        }
        return mapOf("user" to userService.create(form))
    }

    @GetMapping("/v1/users/{loginid}")
    @ResponseStatus(HttpStatus.OK)
    fun getUserId(@PathVariable loginid: String): Map<String, Any?> {
        return mapOf("user" to userService.findByLoginid(loginid))
    }

    @PutMapping("/v1/users/{loginid}")
    @ResponseStatus(HttpStatus.OK)
    fun putUserId(
        @PathVariable loginid: String,
        @RequestBody @Validated form: PutUserIdForm,
        errors: Errors
    ): Map<String, Any?> {
        if (errors.hasErrors()) {
            throw NotValidException(errors.allErrors.map { e -> e.defaultMessage }.toString())
        }
        return mapOf("user" to userService.updateByLoginid(loginid, form))
    }

    @DeleteMapping("/v1/users/{loginid}")
    @ResponseStatus(HttpStatus.OK)
    fun deleteUserId(@PathVariable loginid: String): Map<String, Any?> {
        userService.deleteByLoginid(loginid)
        return mapOf("message" to "success")
    }

    @GetMapping("/v1/users/{loginid}/bots")
    @ResponseStatus(HttpStatus.OK)
    fun getUserIdBots(@PathVariable loginid: String): Map<String, Any?> {
        return mapOf("user" to botService.findByLoginid(loginid))
    }
}
