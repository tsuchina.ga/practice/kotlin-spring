package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Action
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ActionRepository : JpaRepository<Action, Int>
