package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Bot
import ga.tsuchina.botters.v1.repositories.BotRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BotService {

    @Autowired
    lateinit var repository: BotRepository

    @Autowired
    lateinit var userService: UserService

    fun findAll(): List<Bot> = repository.findAll()

    fun findByUserId(userId: Int): List<Bot> = repository.findByUserId(userId)

    fun findByLoginid(loginid: String): List<Bot> = findByUserId(userService.findByLoginid(loginid).id)

    fun findById(id: Int): Bot {
        val bot = repository.findById(id).orElse(null)
        bot?.let { return bot }
        throw NotFoundRecordException()
    }

    /** botの行動を実行する */
    fun action(botId: Int, actionId: Int): Boolean = when (actionId) {
        1 -> true
        else -> false
    }
}
