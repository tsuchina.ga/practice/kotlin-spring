package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Bot
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

@Repository
interface BotRepository : JpaRepository<Bot, Int>, JpaSpecificationExecutor<Bot> {

    fun findByUserId(userId: Int): List<Bot>

    @Transactional
    @Modifying
    @Query("DELETE FROM Bot")
    fun deleteAllRecords()
}
