package ga.tsuchina.botters.v1.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import org.springframework.validation.annotation.Validated
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Min
import javax.validation.constraints.Size

@Entity
@Table(name = "bots")
@Where(clause = "is_deleted = 0")
@Validated
data class Bot (

    @field: Id
    @field: GeneratedValue(strategy = GenerationType.IDENTITY)
    @field: Column(name = "id")
    var id: Int = -1,

    @field: Column(name = "user_id")
    @field: Min(1)
    var userId: Int = 0,

    @field: Column(name = "profession_id")
    var professionId: Int = 0,

    @field: Column(name = "strategy_id")
    var strategyId: Int = 0,

    @field: Column(name = "is_fighter")
    var isFighter: Int = 0,

    @field: Column(name = "name")
    @field: Size(min = 1, max = 16)
    var name: String = "",

    @field: Column(name = "store_size")
    var storeSize: Int = 0,

    @field: Column(name = "is_deleted")
    var isDeleted: Int = 0,

    @field: Column(name = "turn_created_at")
    var turnCreatedAt: Int = 0,

    @field: Column(name = "turn_deleted_at")
    var turnDeletedAt: Int = 0,

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "created_at")
    var createdAt: Date = Date(),

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "updated_at")
    var updatedAt: Date = Date()
)
