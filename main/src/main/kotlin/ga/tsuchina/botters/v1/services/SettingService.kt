package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Setting
import ga.tsuchina.botters.v1.repositories.SettingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SettingService {

    @Autowired
    lateinit var repository: SettingRepository

    fun findAll(): Map<String, String> = repository.findAll().map {
        setting -> (setting.key to setting.value)
    }.toMap()

    fun findByKey(key: String): Pair<String, String> {
        val setting: Setting? = repository.findByKey(key)
        if (setting != null) return (setting.key to setting.value)
        else throw NotFoundRecordException()
    }

    fun addTurn(): Int {
        val setting: Setting = repository.findByKey("turn")
            ?: repository.save(Setting(key = "turn", value = "0"))

        setting.value = (setting.value.toInt() + 1).toString()
        return repository.save(setting).value.toInt()
    }
}
