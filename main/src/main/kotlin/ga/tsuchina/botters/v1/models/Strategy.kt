package ga.tsuchina.botters.v1.models

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Where
import java.util.Date
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@Entity
@Table(name = "strategies")
@Where(clause = "is_deleted = 0")
data class Strategy (

    @field: Id
    @field: GeneratedValue(strategy = GenerationType.IDENTITY)
    @field: Column(name = "id")
    var id: Int = -1,

    @field: Column(name = "user_id")
    @field: NotNull
    @field: Min(1)
    var userId: Int = 0,

    @field: Column(name = "name")
    @field: NotNull
    @field: Size(min = 0, max = 256)
    var name: String = "",

    @field: Column(name = "create_user_id")
    @field: NotNull
    @field: Min(1)
    var createUserId: Int = 0,

    @field: Column(name = "file")
    @field: NotNull
    var file: String = "",

    @field: Column(name = "access")
    @field: NotNull
    var access: Int = 0,

    @field: JsonIgnore
    @field: Column(name = "is_deleted")
    var isDeleted: Int = 0,

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "created_at")
    var createdAt: Date = Date(),

    @field: JsonIgnore
    @field: Temporal(TemporalType.TIMESTAMP)
    @field: Column(name = "updated_at")
    var updatedAt: Date = Date()
)