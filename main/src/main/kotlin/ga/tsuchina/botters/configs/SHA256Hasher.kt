package ga.tsuchina.botters.configs

import java.security.MessageDigest

class SHA256Hasher {

    fun create(loginid: String, password: String): String {

        var hash = ""
        var stretch = password.length
        while (stretch > 0) {
            hash = this.hashString("$loginid:$hash:$password")
            stretch--
        }
        return hash
    }

    private fun hashString(input: String): String {

        val hexChars = "0123456789abcdef"
        val bytes = MessageDigest
            .getInstance("SHA-256")
            .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)

        bytes.forEach {
            val i = it.toInt()
            result.append(hexChars[i shr 4 and 0x0f])
            result.append(hexChars[i and 0x0f])
        }

        return result.toString()
    }
}