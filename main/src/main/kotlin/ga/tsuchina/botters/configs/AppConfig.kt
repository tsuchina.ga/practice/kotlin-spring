package ga.tsuchina.botters.configs

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AppConfig {

    @Bean
    fun sha256Hasher(): SHA256Hasher = SHA256Hasher()
}
