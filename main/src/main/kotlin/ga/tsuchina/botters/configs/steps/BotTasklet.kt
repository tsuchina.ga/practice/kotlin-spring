package ga.tsuchina.botters.configs.steps

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Strategy
import ga.tsuchina.botters.v1.services.ActionService
import ga.tsuchina.botters.v1.services.BotService
import ga.tsuchina.botters.v1.services.StrategyService
import org.codehaus.jettison.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component

@Component
class BotTasklet : Tasklet {

    @Autowired lateinit var botService: BotService
    @Autowired lateinit var strategyService: StrategyService
    @Autowired lateinit var actionService: ActionService

    override fun execute(contribution: StepContribution?, chunkContext: ChunkContext?): RepeatStatus {

        LoggerFactory.getLogger(BotTasklet::class.java)
            .info("start")

        val executor = java.util.concurrent.Executors.newFixedThreadPool(4)

        for (bot in botService.findAll()) {
            executor.submit({
                LoggerFactory.getLogger(BotTasklet::class.java)
                    .info("start bot: $bot")

                val strategy: Strategy? =
                    try { strategyService.findById(bot.strategyId) } catch (e: NotFoundRecordException) { null }

                if (strategy != null) {
//                  実際に戦略ファイルを読み込んでオブジェクトにパースする処理
                    val path = "strategies/" + strategy.file
                    val resource = ClassPathResource(path)
                    val json = JSONObject(resource.file.readText())

                    LoggerFactory.getLogger(BotTasklet::class.java)
                        .info(bot.id.toString() + ": " + bot.name + ": json: " + json)

//                  パースしたオブジェクトから実行する行動を読み取り決定する処理
//                  その行動が行えるのかをチェックする処理 -> NGならエラーで終わり

                    val action =
                        try { actionService.findById(1) } catch (e: NotFoundRecordException) { null }

                    if (action != null) {
                        val actionResult = botService.action(bot.id, action.id)
//                      結果反映はaction処理の中でやってる

                        if (actionResult) {
                            LoggerFactory.getLogger(BotTasklet::class.java)
                                .info(bot.id.toString() + ": " + bot.name + ": " + action.name + ": success!!")
                        } else {
                            LoggerFactory.getLogger(BotTasklet::class.java)
                                .info(bot.id.toString() + ": " + bot.name + ": " + action.name + ": failed...")
                        }
                    } else {
                        LoggerFactory.getLogger(BotTasklet::class.java)
                            .info(bot.id.toString() + ": " + bot.name + ": not found action")
                    }
                } else {
                    LoggerFactory.getLogger(BotTasklet::class.java)
                        .info(bot.id.toString() + ": " + bot.name + ": not found strategy record")
                }

                LoggerFactory.getLogger(BotTasklet::class.java)
                    .info("end bot: $bot")
            })
        }

        executor.shutdown()

        while (!executor.isTerminated) {
//            LoggerFactory.getLogger(BotTasklet::class.java)
//                .info("thread is alive")
        }

        LoggerFactory.getLogger(BotTasklet::class.java)
            .info("end")

        return RepeatStatus.FINISHED
    }
}