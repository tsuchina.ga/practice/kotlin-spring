package ga.tsuchina.botters.configs.steps

import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.stereotype.Component

@Component
class Tasklet2 : Tasklet {
    override fun execute(contribution: StepContribution?, chunkContext: ChunkContext?): RepeatStatus {
        LoggerFactory.getLogger(Tasklet2::class.java).info("Hello World2!")
        System.out.println("Hello World2!")
        return RepeatStatus.FINISHED
    }
}