package ga.tsuchina.botters.configs.steps

import ga.tsuchina.botters.v1.services.SettingService
import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class TurnTasklet : Tasklet {

    @Autowired
    lateinit var settingService: SettingService

    override fun execute(contribution: StepContribution?, chunkContext: ChunkContext?): RepeatStatus {

//        val beforeTrun = try {
//            settingService.findByKey("turn").second.toInt()
//        } catch (e: NotFoundRecordException) { 0 }
//
//        LoggerFactory.getLogger(TurnTasklet::class.java)
//            .info("before turn: $beforeTrun")

        val turn = settingService.addTurn()

        LoggerFactory.getLogger(TurnTasklet::class.java)
            .info("start turn: $turn")
        return RepeatStatus.FINISHED
    }
}