package ga.tsuchina.botters.configs

import ga.tsuchina.botters.configs.steps.BotTasklet
import ga.tsuchina.botters.configs.steps.TurnTasklet
import org.springframework.batch.core.Job
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.launch.support.SimpleJobLauncher
import org.springframework.batch.core.repository.JobRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.Scheduled

@Configuration
@EnableBatchProcessing
class BatchApplicationConfig(val jobBuilderFactory: JobBuilderFactory, val stepBuilderFactory: StepBuilderFactory) {

    @Autowired
    lateinit var jobLauncher: SimpleJobLauncher

    @Autowired
    lateinit var turnTasklet: TurnTasklet

    @Autowired
    lateinit var botTasklet: BotTasklet

//    @Autowired
//    lateinit var tasklet2: Tasklet2

    @Bean
    fun jobLauncher(jobRepository: JobRepository): SimpleJobLauncher {
        val launcher = SimpleJobLauncher()
        launcher.setJobRepository(jobRepository)
        return launcher
    }

    @Bean
    fun turnStep(): Step? = stepBuilderFactory.get("TURN")?.tasklet(turnTasklet)?.build()

    @Bean
    fun getBotStep(): Step? = stepBuilderFactory.get("GETBOT")?.tasklet(botTasklet)?.build()

//    @Bean
//    fun step2(): Step? = stepBuilderFactory.get("step2")?.tasklet(tasklet2)?.build()

    @Bean
    fun job1(): Job? = jobBuilderFactory
        .get("job1")
        ?.start(turnStep())
        ?.next(getBotStep())
        ?.build()

//    @Bean
//    fun job2(): Job? = jobBuilderFactory.get("job2")?.start(step2())?.build()

    @Scheduled(fixedRate = 10 * 1000)
    @Throws(Exception::class)
    fun perform1() {
        jobLauncher.run(job1(), JobParametersBuilder().addString("JobID", System.currentTimeMillis().toString())
            .toJobParameters())
    }

//    @Scheduled(fixedRate = 5000)
//    @Throws(Exception::class)
//    fun perform2() {
//        jobLauncher.run(job2(), JobParametersBuilder().addString("JobID", System.currentTimeMillis().toString())
//            .toJobParameters())
//    }
}
