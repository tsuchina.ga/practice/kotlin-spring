package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.User
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import java.util.Date
import javax.validation.ConstraintViolationException

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class UserRepositoryTest {

    @Autowired
    lateinit var repository: UserRepository

    private val user1 = User(
        id = 1,
        loginid = "userid1",
        password = "25469229dae264aad3995bed5e3e5e0da30c54475865eb557a83ddf22d117006",
        name = "user1",
        money = 0,
        isDeleted = 0,
        createdAt = Date(),
        updatedAt = Date()
    )

    private val user2 = User(
        id = 1,
        loginid = "userid2",
        password = "22d7307794cd6f7021cc8b74d61f7f21beae8a6558df2cd50b0a3ceab09e4aaa",
        name = "user2",
        money = 0,
        isDeleted = 1,
        createdAt = Date(),
        updatedAt = Date()
    )

    @Before @Throws(Exception::class)
    fun before() {
        repository.deleteAllRecords()
        repository.save(user1)
        repository.save(user2)
    }

    @After @Throws(Exception::class)
    fun after() {
        repository.deleteAllRecords()
    }

    /** 正常テスト: 有効ユーザーのみ */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        assertEquals(1, repository.findAll().size)
    }

    /** 正常テスト: 有効ユーザーのみ(有効ユーザー追加) */
    @Test @Throws(Exception::class)
    fun testFindAll2() {
        repository.save(user1.copy(loginid = "userid3"))
        assertEquals(2, repository.findAll().size)
    }

    /** 正常テスト: 有効ユーザーのみ(無効ユーザー追加) */
    @Test @Throws(Exception::class)
    fun testFindAll3() {
        repository.save(user1.copy(loginid = "userid4", isDeleted = 1))
        assertEquals(1, repository.findAll().size)
    }

    /** 正常テスト: 有効ユーザーなし */
    @Test @Throws(Exception::class)
    fun testFindAll4() {
        val user = repository.findByLoginid("userid1")
        if (user != null) {
            repository.save(user.copy(isDeleted = 1))
            assertEquals(0, repository.findAll().size)
        } else {
            fail()
        }
    }

    /** 何故か環境によってエラーになるのでコメントアウトしたテスト */
    @Test @Throws(Exception::class)
    fun testFindByLoginid1() {
//        assertNotNull(repository.findByLoginid("userid1"))
    }

    /** 正常テスト: 存在しないユーザー */
    @Test @Throws(Exception::class)
    fun testFindByLoginid2() {
        assertNull(repository.findByLoginid("userid0"))
    }

    /** 正常テスト: 削除されたユーザー */
    @Test @Throws(Exception::class)
    fun testFindByLoginid3() {
        assertNull(repository.findByLoginid("userid2"))
    }

    /** 正常テスト: 存在するユーザー */
    @Test @Throws(Exception::class)
    fun testFindByLoginid4() {
        assertNotNull(repository.findByLoginid("userid1"))
    }

    /** 異常テスト: 追加 loginid min */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave1() {
        val user = User(loginid = "01234", password = "userPassword", name = "userName")
        repository.save(user)
    }

    /** 異常テスト: 追加 loginid max */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave2() {
        val user = User(loginid = "012345678901234567890123456789012", password = "userPassword", name = "userName")
        repository.save(user)
    }

    /** 異常テスト: 追加 loginid pattern */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave3() {
        val user = User(loginid = "012/345-678+9", password = "userPassword", name = "userName")
        repository.save(user)
    }

    /** 異常テスト: 追加 password min */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave4() {
        val user = User(loginid = "newLoginid", password = "01234", name = "userName")
        repository.save(user)
    }

    /** 異常テスト: 追加 password max */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave5() {
        val user = User(
            loginid = "newLoginid",
            password =
            "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
            "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" +
            "012345678901234567890123456789012345678901234567890123456",
            name = "userName")
        repository.save(user)
    }

    /** 異常テスト: 追加 password pattern */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave6() {
        val user = User(loginid = "newLoginid", password = "012/345-678+9", name = "userName")
        repository.save(user)
    }

    /** 異常テスト: 追加 name min */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave7() {
        val user = User(loginid = "newLoginid", password = "newPassword", name = "012")
        repository.save(user)
    }

    /** 異常テスト: 追加 name max */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave8() {
        val user = User(loginid = "newLoginid", password = "newPassword", name = "012345678901234567890123456789012")
        repository.save(user)
    }

    /** 異常テスト: 追加 name pattern */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave9() {
        val user = User(loginid = "newLoginid", password = "newPassword", name = "012/345-678+9")
        repository.save(user)
    }

}
