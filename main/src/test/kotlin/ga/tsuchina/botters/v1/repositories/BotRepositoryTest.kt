package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Bot
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import java.util.Date
import javax.validation.ConstraintViolationException

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class BotRepositoryTest {

    @Autowired
    lateinit var repository: BotRepository

    var bot1 = Bot(userId = 1, name = "bot1")
    var bot2 = Bot(userId = 1, name = "bot2")
    var bot3 = Bot(userId = 2, name = "bot3")
    var bot4 = Bot(userId = 2, name = "bot4", isDeleted = 1)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        repository.deleteAllRecords()
        bot1 = repository.save(bot1)
        bot2 = repository.save(bot2)
        bot3 = repository.save(bot3)
        bot4 = repository.save(bot4)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAllRecords()
    }

    /** 正常テスト: 有効レコード数 */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        assertEquals(3, repository.findAll().size)
    }

    /** 正常テスト: レコード全削除 */
    @Test @Throws(Exception::class)
    fun testDeleteAllRecords1() {
        assertEquals(3, repository.findAll().size)
        repository.deleteAllRecords()
        assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト: レコード削除 */
    @Test @Throws(Exception::class)
    fun testDelete1() {
        assertEquals(3, repository.findAll().size)
        repository.delete(bot1)
        assertEquals(2, repository.findAll().size)
        repository.delete(bot2)
        assertEquals(1, repository.findAll().size)
    }

    /** 正常テスト: 追加 */
    @Test @Throws(Exception::class)
    fun testSave1() {
        assertEquals(3, repository.findAll().size)

        val bot = Bot(userId = 1, name = "newBot")
        val addedBot = repository.save(bot)

        assertEquals(4, repository.findAll().size)
        assertEquals(bot.userId, addedBot.userId)
        assertEquals(bot.name, addedBot.name)
    }

    /** 正常テスト: 更新 */
    @Test @Throws(Exception::class)
    fun testSave2() {
        assertEquals(3, repository.findAll().size)

        val bot = bot1.copy(name = "newBot")
        val addedBot = repository.save(bot)

        assertEquals(3, repository.findAll().size)
        assertEquals(bot1.userId, addedBot.userId)
        assertEquals(bot.name, addedBot.name)
    }

    /** 異常テスト: user_id min */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave3() {
        val bot = Bot(userId = -1, name = "newBot")
        repository.save(bot)
    }

    /** 異常テスト: name min */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave4() {
        val bot = Bot(userId = 1, name = "")
        repository.save(bot)
    }

    /** 異常テスト: name max */
    @Test(expected = ConstraintViolationException::class) @Throws(Exception::class)
    fun testSave5() {
        val bot = Bot(userId = 1, name = "0123456789abcdefghijklmnopqestyvwxyz")
        repository.save(bot)
    }

    /** 正常テスト: user_idに紐づくレコード */
    @Test @Throws(Exception::class)
    fun testFindByUserId1() {
        assertEquals(2, repository.findByUserId(1).size)
    }

    /** 正常テスト: 削除されたレコードは紐づかない */
    @Test @Throws(Exception::class)
    fun testFindByUserId2() {
        assertEquals(1, repository.findByUserId(2).size)
    }

    /** 正常テスト: 紐づきが0件でもエラーにならない */
    @Test @Throws(Exception::class)
    fun testFindByUserId3() {
        assertEquals(0, repository.findByUserId(3).size)
    }

    /** 正常テスト: 存在するid */
    @Test @Throws(Exception::class)
    fun testFindById1() {
        val date = Date()
        assertEquals(
            bot1.copy(createdAt = date, updatedAt = date),
            repository.findById(bot1.id).get().copy(createdAt = date, updatedAt = date)
        )
    }

    /** 正常テスト: 削除されたid */
    @Test @Throws(Exception::class)
    fun testFindById2() {
        assertNull(repository.findById(bot4.id).orElse(null))
    }

    /** 正常テスト: 存在しないid */
    @Test @Throws(Exception::class)
    fun testFindById3() {
        assertNull(repository.findById(0).orElse(null))
    }

}
