package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Strategy
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class StrategyRepositoryTest {

    @Autowired
    lateinit var repository: StrategyRepository

    var strategy1 = Strategy(userId = 1, createUserId = 1)
    var strategy2 = Strategy(userId = 1, createUserId = 2)
    var strategy3 = Strategy(userId = 2, createUserId = 2)
    var strategy4 = Strategy(userId = 2, createUserId = 2, isDeleted = 1)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        repository.deleteAllRecords()
        strategy1 = repository.save(strategy1)
        strategy2 = repository.save(strategy2)
        strategy3 = repository.save(strategy3)
        strategy4 = repository.save(strategy4)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAllRecords()
    }

    /** 正常テスト: 有効レコード数 */
    @Test
    @Throws(Exception::class)
    fun testFindAll1() {
        assertEquals(3, repository.findAll().size)
    }

    /** 正常テスト: 有効レコード */
    @Test
    @Throws(Exception::class)
    fun testFindById1() {
        assertEquals(strategy1.id, repository.findById(strategy1.id).get().id)
    }

    /** 正常テスト: 削除済みレコード */
    @Test
    @Throws(Exception::class)
    fun testFindById2() {
        assertNull(repository.findById(strategy4.id).orElse(null))
    }

    /** 正常テスト: 存在しないレコード */
    @Test
    @Throws(Exception::class)
    fun testFindById3() {
        assertNull(repository.findById(0).orElse(null))
    }

    /** 正常テスト: レコード全削除 */
    @Test @Throws(Exception::class)
    fun testDeleteAllRecords1() {
        assertEquals(3, repository.findAll().size)
        repository.deleteAllRecords()
        assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト: レコード削除 */
    @Test @Throws(Exception::class)
    fun testDelete1() {
        assertEquals(3, repository.findAll().size)
        repository.delete(strategy3)
        assertEquals(2, repository.findAll().size)
        repository.delete(strategy4)
        assertEquals(2, repository.findAll().size)
    }

    /** 正常テスト: 削除済みレコード削除 */
    @Test @Throws(Exception::class)
    fun testDelete2() {
        assertEquals(3, repository.findAll().size)
        repository.delete(strategy4)
        assertEquals(3, repository.findAll().size)
    }
}