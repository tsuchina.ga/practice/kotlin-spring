package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Setting
import ga.tsuchina.botters.v1.repositories.SettingRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class SettingServiceTest {

    @Autowired
    lateinit var repository: SettingRepository

    @Autowired
    lateinit var service: SettingService

    var setting1 = Setting("turn", "5")
    var setting2 = Setting("user", "30")

    @Before @Throws(Exception::class)
    fun setUp() {
        repository.deleteAll()

        setting1 = repository.save(setting1)
        setting2 = repository.save(setting2)
    }

    @After @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAll()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        val map = mapOf(setting1.key to setting1.value, setting2.key to setting2.value)
        assertEquals(map, service.findAll())
    }

    /** 正常テスト: 0件 */
    @Test @Throws(Exception::class)
    fun testFindAll2() {
        repository.deleteAll()
        assertEquals(0, service.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindByKey1() {
        assertEquals((setting1.key to setting1.value), service.findByKey(setting1.key))
        assertEquals((setting2.key to setting2.value), service.findByKey(setting2.key))
    }

    /** 異常テスト: 存在しないkey */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindByKey2() {
        service.findByKey("")
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testAddTurn1() {
        assertEquals(6, service.addTurn())
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testAddTurn2() {
        repository.deleteAll()
        assertEquals(1, service.addTurn())
    }

}
