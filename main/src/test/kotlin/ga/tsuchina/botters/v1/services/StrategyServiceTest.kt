package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Strategy
import ga.tsuchina.botters.v1.repositories.StrategyRepository
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.assertEquals
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import kotlin.math.exp

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class StrategyServiceTest {

    @Autowired
    lateinit var repository: StrategyRepository

    @Autowired
    lateinit var service: StrategyService

    var strategy1 = Strategy(userId = 1, createUserId = 1)
    var strategy2 = Strategy(userId = 1, createUserId = 2)
    var strategy3 = Strategy(userId = 2, createUserId = 2)
    var strategy4 = Strategy(userId = 2, createUserId = 2, isDeleted = 1)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        repository.deleteAllRecords()
        strategy1 = repository.save(strategy1)
        strategy2 = repository.save(strategy2)
        strategy3 = repository.save(strategy3)
        strategy4 = repository.save(strategy4)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAllRecords()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindById1() {
        assertEquals(strategy1.id, service.findById(strategy1.id).id)
    }

    /** 異常テスト: 削除済み */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindById2() {
        service.findById(strategy4.id)
    }

    /** 異常テスト: 存在しない */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindById3() {
        service.findById(0)
    }

}
