package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Action
import ga.tsuchina.botters.v1.repositories.ActionRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class ActionServiceTest {

    @Autowired lateinit var repository: ActionRepository
    @Autowired lateinit var service: ActionService

    var action1 = Action(name = "action1")
    var action2 = Action(name = "action2")

    @Before @Throws(Exception::class)
    fun setUp() {
        repository.deleteAll()
        action1 = repository.save(action1)
        action2 = repository.save(action2)
    }

    @After @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAll()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindById1() {
        assertEquals(action1, service.findById(action1.id))
    }

    /** 異常テスト: 存在しないid */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindById2() {
        service.findById(0)
    }

}
