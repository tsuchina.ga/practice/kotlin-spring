package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Profession
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class ProfessionRepositoryTest {

    @Autowired
    lateinit var repository: ProfessionRepository

    var profession1 = Profession(name = "profession1")
    var profession2 = Profession(name = "profession2")

    @Before
    @Throws(Exception::class)
    fun setUp() {
        repository.deleteAll()
        profession1 = repository.save(profession1)
        profession2 = repository.save(profession2)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAll()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        assertEquals(2, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindById1() {
        assertEquals(profession1, repository.findById(profession1.id).get())
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindById2() {
        assertNull(repository.findById(0).orElse(null))
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAll1() {
        assertEquals(2, repository.findAll().size)
        repository.deleteAll()
        assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAll2() {
        repository.deleteAll()
        assertEquals(0, repository.findAll().size)
        repository.deleteAll()
        assertEquals(0, repository.findAll().size)
    }
}