package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.DuplicateRecordException
import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.forms.PostUserForm
import ga.tsuchina.botters.v1.forms.PutUserIdForm
import ga.tsuchina.botters.v1.models.User
import ga.tsuchina.botters.v1.repositories.UserRepository
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import java.util.Date

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class UserServiceTest {

    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var service: UserService

    private val user1 = User(
        id = 1,
        loginid = "userid1",
        password = "25469229dae264aad3995bed5e3e5e0da30c54475865eb557a83ddf22d117006",
        name = "user1",
        money = 0,
        isDeleted = 0,
        createdAt = Date(),
        updatedAt = Date()
    )

    private val user2 = User(
        id = 1,
        loginid = "userid2",
        password = "22d7307794cd6f7021cc8b74d61f7f21beae8a6558df2cd50b0a3ceab09e4aaa",
        name = "user2",
        money = 0,
        isDeleted = 1,
        createdAt = Date(),
        updatedAt = Date()
    )

    @Before @Throws(Exception::class)
    fun before() {
        repository.deleteAllRecords()
        repository.save(user1)
        repository.save(user2)
    }

    @After @Throws(Exception::class)
    fun after() {
        repository.deleteAllRecords()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        Assert.assertEquals(1, service.findAll().size)
    }

    /** 正常テスト: ユーザー追加後 */
    @Test @Throws(Exception::class)
    fun testFindAll2() {
        repository.save(user1.copy(loginid = "userid3"))
        Assert.assertEquals(2, service.findAll().size)
    }

    /** 正常テスト: ユーザー全削除 */
    @Test @Throws(Exception::class)
    fun testFindAll3() {
        repository.deleteAllRecords()
        Assert.assertEquals(0, service.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindByLoginid1() {
        Assert.assertEquals(user1, service.findByLoginid("userid1").copy(id = 1))
    }

    /** 異常テスト: 削除されたユーザー */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindByLoginid2() {
        service.findByLoginid("userid2")
    }

    /** 異常テスト: 存在しないユーザー */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindByLoginid3() {
        service.findByLoginid("userid3")
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testCreate1() {
        val form = PostUserForm(
            loginid = "newUserId",
            password = "password",
            name = "name"
        )

        Assert.assertEquals("newUserId", service.create(form).loginid)
    }

    /** 正常テスト: 削除されたユーザのloginidで登録 */
    @Test @Throws(Exception::class)
    fun testCreate2() {
        val form = PostUserForm(
            loginid = "userid2",
            password = "password",
            name = "name"
        )

        Assert.assertEquals("userid2", service.create(form).loginid)
    }

    /** 異常テスト: 有効なloginidで登録 */
    @Test(expected = DuplicateRecordException::class) @Throws(Exception::class)
    fun testCreate3() {
        service.create(PostUserForm(
            loginid = "userid1",
            password = "password",
            name = "name"
        ))
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testUpdateByLoginid1() {
        val form = PutUserIdForm(
            password = "newPassword",
            name = "newName"
        )
        Assert.assertEquals("newName", service.updateByLoginid("userid1", form).name)
    }

    /** 正常テスト: 部分的な更新 */
    @Test @Throws(Exception::class)
    fun testUpdateByLoginid2() {
        val form = PutUserIdForm(name = "newName")
        Assert.assertEquals("newName", service.updateByLoginid("userid1", form).name)
    }

    /** 正常テスト: 更新情報なし */
    @Test @Throws(Exception::class)
    fun testUpdateByLoginid3() {
        val form = PutUserIdForm()
        Assert.assertEquals("user1", service.updateByLoginid("userid1", form).name)
    }

    /** 異常テスト: 存在しないユーザ */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testUpdateByLoginid4() {
        service.updateByLoginid("userid0", PutUserIdForm())
    }

    /** 異常テスト: 削除されたユーザ */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testUpdateByLoginid5() {
        service.updateByLoginid("userid2", PutUserIdForm())
    }

    /** 正常テスト: パスワードがハッシュ化されていること */
    @Test@Throws(Exception::class)
    fun testUpdateByLoginid6() {
        val form = PutUserIdForm(password = "passwordpassword")
        Assert.assertNotEquals(
            "passwordpassword",
            service.updateByLoginid("userid1", form).password
        )
    }

    /** 正常テスト: 更新日時が更新されていること */
    @Test@Throws(Exception::class)
    fun testUpdateByLoginid7() {
        val form = PutUserIdForm()
        Assert.assertNotEquals(user1.updatedAt, service.updateByLoginid("userid1", form).updatedAt)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteByLoginid1() {
        service.deleteByLoginid("userid1")
        Assert.assertEquals(0, service.findAll().size)
    }

    /** 異常テスト: 存在しないユーザー */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testDeleteByLoginid2() {
        service.deleteByLoginid("userid0")
    }

    /** 異常テスト: 削除されたユーザー */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testDeleteByLoginid3() {
        service.deleteByLoginid("userid2")
    }
}
