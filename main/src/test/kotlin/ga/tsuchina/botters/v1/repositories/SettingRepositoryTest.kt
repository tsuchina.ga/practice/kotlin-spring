package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Setting
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.transaction.TransactionSystemException

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class SettingRepositoryTest {

    @Autowired
    lateinit var repository: SettingRepository

    var setting1 = Setting("turn", "5")
    var setting2 = Setting("hoge", "hogehoge")

    @Before
    @Throws(Exception::class)
    fun before() {
        repository.deleteAll()
        repository.save(setting1)
        repository.save(setting2)
    }

    @After
    @Throws(Exception::class)
    fun after() {
        repository.deleteAll()
    }

    /** 正常テスト: すべての設定 */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        assertEquals(2, repository.findAll().size)
    }

    /** 正常テスト: 存在するキー */
    @Test @Throws(Exception::class)
    fun testFindByKey1() {
        assertEquals(setting1, repository.findByKey("turn"))
    }

    /** 正常テスト: 存在しないキー */
    @Test @Throws(Exception::class)
    fun testFindByKey2() {
        assertNull(repository.findByKey(""))
    }

    /** 正常テスト: 削除 */
    @Test @Throws(Exception::class)
    fun testDelete1() {
        assertNotNull(repository.findByKey("hoge"))
        repository.delete(repository.findByKey("hoge")!!)
        assertNull(repository.findByKey("hoge"))
    }

    /** 正常テスト: 存在しないsetting */
    @Test @Throws(Exception::class)
    fun testDelete2() {
        val hoge = repository.findByKey("hoge")!!
        assertNotNull(repository.findByKey("hoge"))
        repository.delete(hoge)
        assertNull(repository.findByKey("hoge"))
        repository.delete(hoge)
        assertNull(repository.findByKey("hoge"))
    }

    /** 正常テスト: 追加 */
    @Test @Throws(Exception::class)
    fun testSave1() {
        val fuga = Setting(key = "fuga", value = "fugafuga")
        assertEquals(fuga, repository.save(fuga))
    }

    /** 正常テスト: 更新 */
    @Test @Throws(Exception::class)
    fun testSave2() {
        val hoge = setting2.copy(value = "hogehogehoge")
        assertEquals(hoge, repository.save(hoge))
    }

    /** 異常テスト: key notEmpty */
    @Test(expected = TransactionSystemException::class) @Throws(Exception::class)
    fun testSave3() {
        val setting= Setting(key = "", value = "fugafuga")
        repository.save(setting)
    }

}
