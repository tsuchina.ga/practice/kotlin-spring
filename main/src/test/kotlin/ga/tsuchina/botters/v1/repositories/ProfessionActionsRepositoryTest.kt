package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.ProfessionActions
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class ProfessionActionsRepositoryTest {

    @Autowired
    lateinit var repository: ProfessionActionsRepository

    var pa1 = ProfessionActions(professionId = 1, actionId = 1, cost = 1)
    var pa2 = ProfessionActions(professionId = 1, actionId = 2, cost = 1)
    var pa3 = ProfessionActions(professionId = 2, actionId = 1, cost = 1)

    @Before
    @Throws(Exception::class)
    fun setUp() {
        repository.deleteAll()
        pa1 = repository.save(pa1)
        pa2 = repository.save(pa2)
        pa3 = repository.save(pa3)
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAll()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        Assert.assertEquals(3, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAll1() {
        Assert.assertEquals(3, repository.findAll().size)
        repository.deleteAll()
        Assert.assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAll2() {
        repository.deleteAll()
        Assert.assertEquals(0, repository.findAll().size)
        repository.deleteAll()
        Assert.assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindByProfessionId1() {
        Assert.assertEquals(2, repository.findByProfessionId(pa1.professionId).size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindByProfessionId2() {
        Assert.assertEquals(1, repository.findByProfessionId(pa3.professionId).size)
    }

    /** 正常テスト: 存在しないid */
    @Test @Throws(Exception::class)
    fun testFindByProfessionId3() {
        Assert.assertEquals(0, repository.findByProfessionId(0).size)
    }

}