package ga.tsuchina.botters.v1.controllers

import ga.tsuchina.botters.v1.aspects.ExceptionHandler
import ga.tsuchina.botters.v1.models.Setting
import ga.tsuchina.botters.v1.repositories.SettingRepository
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class SettingsControllerTest {

    @Autowired
    lateinit var controller: SettingsController

    @Autowired
    lateinit var repository: SettingRepository

    @Autowired
    lateinit var exceptionHandler: ExceptionHandler

    var mvc: MockMvc? = null
    var setting1 = Setting("turn", "5")
    var setting2 = Setting("user", "30")

    @Before @Throws(Exception::class)
    fun setUp() {
        mvc = MockMvcBuilders
            .standaloneSetup(controller)
            .setControllerAdvice(exceptionHandler)
            .build()

        repository.deleteAll()

        setting1 = repository.save(setting1)
        setting2 = repository.save(setting2)
    }

    @After @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAll()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testGet1() {
        mvc!!.perform(get("/v1/settings")).andExpect(status().isOk)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testGet2() {
        repository.deleteAll()
        mvc!!.perform(get("/v1/settings")).andExpect(status().isOk)
    }

}
