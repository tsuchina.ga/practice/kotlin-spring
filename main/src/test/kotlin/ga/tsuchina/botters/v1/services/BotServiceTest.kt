package ga.tsuchina.botters.v1.services

import ga.tsuchina.botters.v1.exceptions.NotFoundRecordException
import ga.tsuchina.botters.v1.models.Bot
import ga.tsuchina.botters.v1.models.User
import ga.tsuchina.botters.v1.repositories.BotRepository
import ga.tsuchina.botters.v1.repositories.UserRepository
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class BotServiceTest {

    @Autowired
    lateinit var repository: BotRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var service: BotService

    var user1 = User(loginid = "userid1", password = "userid1", name = "userid1")
    var user2 = User(loginid = "userid2", password = "userid2", name = "userid2")
    var user3 = User(loginid = "userid3", password = "userid3", name = "userid3", isDeleted = 1)

    var bot1 = Bot(userId = 1, name = "bot1")
    var bot2 = Bot(userId = 1, name = "bot2")
    var bot3 = Bot(userId = 2, name = "bot3")
    var bot4 = Bot(userId = 2, name = "bot4", isDeleted = 1)
    var bot5 = Bot(userId = 3, name = "bot5")
    var bot6 = Bot(userId = 3, name = "bot6")

    @Before
    @Throws(Exception::class)
    fun setUp() {
        userRepository.deleteAllRecords()
        repository.deleteAllRecords()

        user1 = userRepository.save(user1)
        user2 = userRepository.save(user2)
        user3 = userRepository.save(user3)

        bot1 = repository.save(bot1.copy(userId = user1.id))
        bot2 = repository.save(bot2.copy(userId = user1.id))
        bot3 = repository.save(bot3.copy(userId = user2.id))
        bot4 = repository.save(bot4.copy(userId = user2.id))
        bot5 = repository.save(bot5.copy(userId = user3.id))
        bot6 = repository.save(bot6.copy(userId = user3.id))
    }

    @After
    @Throws(Exception::class)
    fun tearDown() {
        userRepository.deleteAllRecords()
        repository.deleteAllRecords()
    }

    /** 正常テスト: user_idに紐づくレコード */
    @Test @Throws(Exception::class)
    fun testFindByUserId1() {
        assertEquals(2, service.findByUserId(user1.id).size)
    }

    /** 正常テスト: 削除されたレコードは紐づかない */
    @Test @Throws(Exception::class)
    fun testFindByUserId2() {
        assertEquals(1, service.findByUserId(user2.id).size)
    }

    /** 正常テスト: 0件でもエラーにならない */
    @Test @Throws(Exception::class)
    fun testFindByUserId3() {
        assertEquals(0, service.findByUserId(0).size)
    }

    /** 正常テスト: 存在するレコード */
    @Test @Throws(Exception::class)
    fun testFindById1() {
        assertEquals(bot1.name, service.findById(bot1.id).name)
    }

    /** 異常テスト: 削除されたレコード */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindById2() {
        service.findById(bot4.id)
    }

    /** 異常テスト: 存在しないレコード */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindById3() {
        service.findById(0)
    }

    /** 正常テスト: loginidに紐づくレコード */
    @Test @Throws(Exception::class)
    fun testFindByLoginid1() {
        assertEquals(user1.id, bot1.userId)
        assertEquals(user1.id, bot2.userId)
        assertEquals(user2.id, bot3.userId)
        assertEquals(user2.id, bot4.userId)
        assertEquals(user3.id, bot5.userId)
        assertEquals(user3.id, bot6.userId)
        assertEquals(2, service.findByLoginid(user1.loginid).size)
    }

    /** 正常テスト: loginidに紐づく有効なレコード */
    @Test @Throws(Exception::class)
    fun testFindByLoginid2() {
        assertEquals(user1.id, bot1.userId)
        assertEquals(user1.id, bot2.userId)
        assertEquals(user2.id, bot3.userId)
        assertEquals(user2.id, bot4.userId)
        assertEquals(user3.id, bot5.userId)
        assertEquals(user3.id, bot6.userId)
        assertEquals(1, service.findByLoginid(user2.loginid).size)
    }

    /** 異常テスト: 削除されたloginid */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindByLoginid3() {
        service.findByLoginid(user3.loginid)
    }

    /** 異常テスト: 存在しないloginid */
    @Test(expected = NotFoundRecordException::class) @Throws(Exception::class)
    fun testFindByLoginid4() {
        service.findByLoginid(loginid = "userid0")
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        assertEquals(5, service.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll2() {
        repository.deleteAllRecords()
        assertEquals(0, service.findAll().size)
    }

    /** 正常テスト: 存在しない行動 */
    @Test @Throws(Exception::class)
    fun testAction1() {
        assertEquals(false, service.action(bot1.id, 0))
    }

    /** 正常テスト: 休憩 */
    @Test @Throws(Exception::class)
    fun testAction2() {
        assertEquals(true, service.action(bot1.id, 1))
    }

}
