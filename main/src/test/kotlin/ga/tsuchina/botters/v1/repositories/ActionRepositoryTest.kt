package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.Action
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class ActionRepositoryTest {

    @Autowired
    lateinit var repository: ActionRepository

    var action1 = Action(name = "action1")
    var action2 = Action(name = "action2")

    @Before @Throws(Exception::class)
    fun setUp() {
        repository.deleteAll()
        action1 = repository.save(action1)
        action2 = repository.save(action2)
    }

    @After @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAll()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        Assert.assertEquals(2, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindById1() {
        Assert.assertEquals(action1, repository.findById(action1.id).get())
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindById2() {
        Assert.assertNull(repository.findById(0).orElse(null))
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAll1() {
        Assert.assertEquals(2, repository.findAll().size)
        repository.deleteAll()
        Assert.assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAll2() {
        repository.deleteAll()
        Assert.assertEquals(0, repository.findAll().size)
        repository.deleteAll()
        Assert.assertEquals(0, repository.findAll().size)
    }

}