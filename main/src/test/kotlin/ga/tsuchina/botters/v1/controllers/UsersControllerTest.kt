package ga.tsuchina.botters.v1.controllers

import ga.tsuchina.botters.v1.aspects.ExceptionHandler
import ga.tsuchina.botters.v1.models.Bot
import ga.tsuchina.botters.v1.models.User
import ga.tsuchina.botters.v1.repositories.BotRepository
import ga.tsuchina.botters.v1.repositories.UserRepository
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.util.Date

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class UsersControllerTest {

    @Autowired
    lateinit var controller: UsersController

    @Autowired
    lateinit var repository: UserRepository

    @Autowired
    lateinit var botRepository: BotRepository

    @Autowired
    lateinit var exceptionHandler: ExceptionHandler

    private var mvc: MockMvc? = null

    var user1 = User(
        id = 1,
        loginid = "userid1",
        password = "25469229dae264aad3995bed5e3e5e0da30c54475865eb557a83ddf22d117006",
        name = "user1",
        money = 0,
        isDeleted = 0,
        createdAt = Date(),
        updatedAt = Date()
    )

    var user2 = User(
        id = 1,
        loginid = "userid2",
        password = "22d7307794cd6f7021cc8b74d61f7f21beae8a6558df2cd50b0a3ceab09e4aaa",
        name = "user2",
        money = 0,
        isDeleted = 1,
        createdAt = Date(),
        updatedAt = Date()
    )

    var bot1 = Bot(userId = 1, name = "bot1")
    var bot2 = Bot(userId = 1, name = "bot2")
    var bot3 = Bot(userId = 2, name = "bot3")
    var bot4 = Bot(userId = 2, name = "bot4", isDeleted = 1)


    @Before @Throws(Exception::class)
    fun before() {
        mvc = MockMvcBuilders
            .standaloneSetup(controller)
            .setControllerAdvice(exceptionHandler)
            .build()

        repository.deleteAllRecords()
        botRepository.deleteAllRecords()

        user1 = repository.save(user1)
        user2 = repository.save(user2)

        bot1 = botRepository.save(bot1.copy(userId = user1.id))
        bot2 = botRepository.save(bot2.copy(userId = user1.id))
        bot3 = botRepository.save(bot3.copy(userId = user2.id))
        bot4 = botRepository.save(bot4.copy(userId = user2.id))
    }

    @After @Throws(Exception::class)
    fun after() {
        repository.deleteAllRecords()
        botRepository.deleteAllRecords()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testGetUser1() {
        mvc!!.perform(get("/v1/users")).andExpect(status().isOk)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testPostUser1() {
        val map = hashMapOf("loginid" to "newUserLoginid", "password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isOk)
    }

    /** 異常テスト: 存在するloginid */
    @Test @Throws(Exception::class)
    fun testPostUser2() {
        val map = hashMapOf("loginid" to "userid1", "password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 正常テスト: 存在するけど削除されているloginid */
    @Test @Throws(Exception::class)
    fun testPostUser3() {
        val map = hashMapOf("loginid" to "userid2", "password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isOk)
    }

    /** 異常テスト: 必須項目不足 loginid */
    @Test @Throws(Exception::class)
    fun testPostUser4() {
        val map = hashMapOf("password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: 必須項目不足 password */
    @Test @Throws(Exception::class)
    fun testPostUser5() {
        val map = hashMapOf("loginid" to "userid1", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: 必須項目不足 name */
    @Test @Throws(Exception::class)
    fun testPostUser6() {
        val map = hashMapOf("loginid" to "userid1", "password" to "newUserPassword")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate loginid 文字数不足 */
    @Test @Throws(Exception::class)
    fun testPostUser7() {
        val map = hashMapOf("loginid" to "123", "password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate loginid 文字数過多 */
    @Test @Throws(Exception::class)
    fun testPostUser8() {
        val map = hashMapOf(
            "loginid" to "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789",
            "password" to "newUserPassword",
            "name" to "newUserName"
        )
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate loginid パターン外文字列 */
    @Test @Throws(Exception::class)
    fun testPostUser9() {
        val map = hashMapOf(
            "loginid" to "123/456-789",
            "password" to "newUserPassword",
            "name" to "newUserName"
        )
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate password 文字数不足 */
    @Test @Throws(Exception::class)
    fun testPostUser10() {
        val map = hashMapOf("loginid" to "newUserLoginid", "password" to "123", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate password 文字数過多 */
    @Test @Throws(Exception::class)
    fun testPostUser11() {
        val map = hashMapOf(
            "loginid" to "newUserLoginid",
            "password" to "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789",
            "name" to "newUserName"
        )
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate password パターン外文字列 */
    @Test @Throws(Exception::class)
    fun testPostUser12() {
        val map = hashMapOf(
            "loginid" to "newUserLoginid",
            "password" to "123/456-789",
            "name" to "newUserName"
        )
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate name 文字数不足 */
    @Test @Throws(Exception::class)
    fun testPostUser13() {
        val map = hashMapOf("loginid" to "newUserLoginid", "password" to "newUserPassword", "name" to "123")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate name 文字数過多 */
    @Test @Throws(Exception::class)
    fun testPostUser14() {
        val map = hashMapOf(
            "loginid" to "newUserLoginid",
            "password" to "newUserPassword",
            "name" to "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789"
        )
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate name パターン外文字列 */
    @Test @Throws(Exception::class)
    fun testPostUser15() {
        val map = hashMapOf(
            "loginid" to "newUserLoginid",
            "password" to "newUserPassword",
            "name" to "123/456-789"
        )
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(post("/v1/users")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testGetUserId1() {
        mvc!!.perform(get("/v1/users/{loginid}", "userid1"))
            .andExpect(status().isOk)
    }

    /** 異常テスト: 存在しないユーザー */
    @Test @Throws(Exception::class)
    fun testGetUserId2() {
        mvc!!.perform(get("/v1/users/{loginid}", "userid0"))
            .andExpect(status().isNotFound)
    }

    /** 異常テスト: 削除されたユーザー */
    @Test @Throws(Exception::class)
    fun testGetUserId3() {
        mvc!!.perform(get("/v1/users/{loginid}", "userid2"))
            .andExpect(status().isNotFound)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testPutUserId1() {
        val map = hashMapOf("password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isOk)
    }

    /** 正常テスト: 部分的な更新 */
    @Test @Throws(Exception::class)
    fun testPutUserId2() {
        val map = hashMapOf("name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isOk)
    }

    /** 正常テスト: 無関係な項目を持つ更新 */
    @Test @Throws(Exception::class)
    fun testPutUserId3() {
        val map = hashMapOf("name" to "newUserName", "namename" to "namenamenamename")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isOk)
    }

    /** 異常テスト: 存在しないユーザー */
    @Test @Throws(Exception::class)
    fun testPutUserId4() {
        val map = hashMapOf("password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid0")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isNotFound)
    }

    /** 異常テスト: 削除されたユーザー */
    @Test @Throws(Exception::class)
    fun testPutUserId5() {
        val map = hashMapOf("password" to "newUserPassword", "name" to "newUserName")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid2")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isNotFound)
    }

    /** 異常テスト: FormValidate password 文字数不足 */
    @Test @Throws(Exception::class)
    fun testPutUserId6() {
        val map = hashMapOf("password" to "123")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate password 文字数過多 */
    @Test @Throws(Exception::class)
    fun testPutUserId7() {
        val map = hashMapOf("password" to "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate password パターン外文字列 */
    @Test @Throws(Exception::class)
    fun testPutUserId8() {
        val map = hashMapOf("password" to "123/456-789")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate name 文字数不足 */
    @Test @Throws(Exception::class)
    fun testPutUserId9() {
        val map = hashMapOf("name" to "123")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate name 文字数過多 */
    @Test @Throws(Exception::class)
    fun testPutUserId10() {
        val map = hashMapOf("name" to "abcdefghijklmnopqrstuvwxyz0123456789abcdefghijklmnopqrstuvwxyz0123456789")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 異常テスト: FormValidate name パターン外文字列 */
    @Test @Throws(Exception::class)
    fun testPutUserId11() {
        val map = hashMapOf("name" to "123/456-789")
        val jsonStr: String = JSONObject(map).toString()

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isBadRequest)
    }

    /** 正常テスト: 更新項目なし */
    @Test @Throws(Exception::class)
    fun testPutUserId12() {
        val jsonStr = "{}"

        mvc!!.perform(put("/v1/users/{loginid}", "userid1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jsonStr)
        ).andExpect(status().isOk)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteUserId1() {
        mvc!!.perform(delete("/v1/users/{loginid}", "userid1"))
            .andExpect(status().isOk)
    }

    /** 異常テスト: 存在しないユーザー */
    @Test @Throws(Exception::class)
    fun testDeleteUserId2() {
        mvc!!.perform(delete("/v1/users/{loginid}", "userid0"))
            .andExpect(status().isNotFound)
    }

    /** 異常テスト: 削除されたユーザー */
    @Test @Throws(Exception::class)
    fun testDeleteUserId3() {
        mvc!!.perform(delete("/v1/users/{loginid}", "userid2"))
            .andExpect(status().isNotFound)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testGetUserIdBots1() {
        mvc!!.perform(get("/v1/users/{loginid}/bots", "userid1"))
            .andExpect(status().isOk)
    }

    /** 異常テスト: 存在しないloginid */
    @Test @Throws(Exception::class)
    fun testGetUserIdBots2() {
        mvc!!.perform(get("/v1/users/{loginid}/bots", "userid0"))
            .andExpect(status().isNotFound)
    }
}
