package ga.tsuchina.botters.v1.repositories

import ga.tsuchina.botters.v1.models.BotProfessions
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
@WebAppConfiguration
class BotProfessionsRepositoryTest {

    @Autowired
    lateinit var repository: BotProfessionsRepository

    var bp1 = BotProfessions(botId = 1, professionId = 1)
    var bp2 = BotProfessions(botId = 1, professionId = 1)
    var bp3 = BotProfessions(botId = 2, professionId = 1, isDeleted = 1)

    @Before @Throws(Exception::class)
    fun setUp() {
        repository.deleteAllRecords()
        bp1 = repository.save(bp1)
        bp2 = repository.save(bp2)
        bp3 = repository.save(bp3)
    }

    @After @Throws(Exception::class)
    fun tearDown() {
        repository.deleteAllRecords()
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindAll1() {
        Assert.assertEquals(2, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAllRecords1() {
        Assert.assertEquals(2, repository.findAll().size)
        repository.deleteAllRecords()
        Assert.assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testDeleteAllRecords2() {
        repository.deleteAllRecords()
        Assert.assertEquals(0, repository.findAll().size)
        repository.deleteAllRecords()
        Assert.assertEquals(0, repository.findAll().size)
    }

    /** 正常テスト */
    @Test @Throws(Exception::class)
    fun testFindByBotId1() {
        Assert.assertEquals(2, repository.findByBotId(bp1.botId).size)
    }

    /** 正常テスト: 削除されたレコード */
    @Test @Throws(Exception::class)
    fun testFindByBotId2() {
        Assert.assertEquals(0, repository.findByBotId(bp3.botId).size)
    }

    /** 正常テスト: 存在しないレコード */
    @Test @Throws(Exception::class)
    fun testFindByBotId3() {
        Assert.assertEquals(0, repository.findByBotId(0).size)
    }

}