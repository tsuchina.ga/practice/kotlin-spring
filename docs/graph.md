```flow
    start=>start
    end=>end

    cond1=>condition: turn = null
    op11=>operation: save turn = 0
    op12=>operation: save turn++

    op2=>operation: get bots
    cond21=>condition: exists bots
    cond22=>condition: thread create(bot)
    op221=>operation: get strategy
    op222=>operation: get action
    op223=>operation: reflect
    op23=>operation: wait thread end

    start->cond1
    cond1(yes, right)->op11->op12
    cond1(no)->op12
    op12->op2
    op2->cond21
    cond21(yes)->cond22
    cond22(yes)->op221->op222->op223
    cond22(no)->cond21
    cond21(no)->op23->end
```
