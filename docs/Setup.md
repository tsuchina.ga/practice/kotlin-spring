# セットアップ

## 開発環境

* Windows10 64bit Pro
* Docker for Windows(最新: Version 18.03.0-ce-win59 (16762))
* IDE: IntelliJ IDEA
* Text Editor: Atom Editor

## 環境構築

### Docker

docker-compose.ymlのあるディレクトリで `docker-compose up -d` を実行。

すると下記コンテナが立つ。

* botters(処理サーバ)
* botters-psql(DBサーバ)

bottersにwebapiやmainを入れて、botters-psqlではDBだけに専念する。


### DBサーバ(PostgreSQL)

セットアップの大半はdocker-compose.ymlに記載済み。

開発と同時に変更が必要なのはsqlディレクトリにあるクエリファイルくらい。

先頭文字の若い順に処理されるので、テーブル構成の生成が1で、2以降にマスターデータを入れる。


### webapi

Kotlin + SpringBoot で構築。

0. 最小構成を取得
  0. [Spring Initializr](https://start.spring.io/)にアクセス
  0. Generate a **Gradle Project** with **Kotlin** and Spring Boot **2.0.0**
  0. Group: **ga.tsuchina.botters**
  0. Artifact: **webapi**
  0. Dependencies: **Web**, **JPA**, **PostgreSQL**
  0. Generate Project
  0. ダウンロードしたZIPを任意のディレクトリに解凍

0. IntelliJでImport
  0. IntelliJを起動し、Import Projectを選択
  0. 解凍したファイルの中にあるgradle.buildを選択
  0. Use auto-importにチェックを入れ、Gradle JVMを指定
  0. BuildがCompliteしたら開発準備完了

0. その他
  0. 操作中にSDKがないと言われたらJDKを指定する


### batch

こちらも同じく Kotlin + SpringBoot (batch) で構築。

0. 最小構成を取得
  0. [Spring Initializr](https://start.spring.io/)にアクセス
  0. Generate a **Gradle Project** with **Kotlin** and Spring Boot **2.0.0**
  0. Group: **ga.tsuchina.botters**
  0. Artifact: **batch**
  0. Dependencies: **Batch**, **JPA**, **PostgreSQL**
  0. Generate Project
  0. ダウンロードしたZIPを任意のディレクトリに解凍

0. IntelliJでImport
  0. IntelliJを起動し、Import Projectを選択
  0. 解凍したファイルの中にあるgradle.buildを選択
  0. Use auto-importにチェックを入れ、Gradle JVMを指定
  0. BuildがCompliteしたら開発準備完了

#### 参考

* [コジオニルク - Spring Boot Batch で Hello World](http://www.kojion.com/posts/799)
