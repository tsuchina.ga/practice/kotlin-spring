-- 設定データ
INSERT INTO settings(key, value) VALUES('turn', '0');

-- ユーザーデータ
INSERT INTO users(loginid, password, name) VALUES('administrator', '', 'Administrator');

-- 職業データ
INSERT INTO professions(name, max_level) VALUES('無職', 1);

-- 行動データ
INSERT INTO actions(name) VALUES('休憩');

-- 職業行動データ
INSERT INTO profession_actions(profession_id, action_id, cost)
SELECT id, (SELECT id FROM actions WHERE name = '休憩'), 0 FROM professions;

-- 戦略データ
INSERT INTO strategies(user_id, create_user_id, name, file)
SELECT id, id, 'NPC戦略01', '1/1523259469236.json' FROM users WHERE loginid = 'administrator';

-- ボットデータ
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC001', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC002', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC003', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC004', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC005', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC006', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC007', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC008', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC009', 1 FROM users WHERE loginid = 'administrator';
INSERT INTO bots(user_id, name, turn_created_at) SELECT id, 'NPC010', 1 FROM users WHERE loginid = 'administrator';

UPDATE bots SET strategy_id = (SELECT id FROM strategies WHERE name = 'NPC戦略01' ), updated_at = CURRENT_TIMESTAMP;
UPDATE bots SET profession_id = (SELECT id FROM professions WHERE name = '無職' ), updated_at = CURRENT_TIMESTAMP;

-- ボット職業データ
INSERT INTO bot_professions(bot_id, profession_id, level)
SELECT id, (SELECT id FROM professions WHERE name = '無職'), 1 FROM bots;
