Drop Table If Exists settings;
CREATE TABLE settings ( key varchar(32) primary key Not Null UNIQUE , value varchar(256) Not Null );
COMMENT ON COLUMN settings.key IS '設定キー' ; COMMENT ON COLUMN settings.value IS '設定値' ; COMMENT ON TABLE settings IS '設定' ;
CREATE INDEX settings_key ON settings ( key ) ;

Drop Table If Exists users;
CREATE TABLE users ( id serial primary key Not Null , loginid varchar(32) Not Null , password varchar(256) Not Null , name varchar(32) Not Null , money int Not Null Default 0 , is_deleted smallint Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN users.id IS 'ユーザーID(シリアル値・自動カウント)' ; COMMENT ON COLUMN users.loginid IS 'ログインID' ; COMMENT ON COLUMN users.password IS 'パスワード(ハッシュ化された値)' ; COMMENT ON COLUMN users.name IS 'ユーザー名' ; COMMENT ON COLUMN users.money IS '所持金' ; COMMENT ON COLUMN users.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN users.created_at IS '登録日時' ; COMMENT ON COLUMN users.updated_at IS '更新日時' ; COMMENT ON TABLE users IS 'ユーザー' ;
CREATE INDEX users_loginid ON users ( loginid ) ; CREATE INDEX users_money ON users ( money ) ;

Drop Table If Exists bots;
CREATE TABLE bots ( id serial primary key Not Null , user_id int Not Null , profession_id int Not Null Default 0 , strategy_id int Not Null Default 0 , is_fighter smallint Not Null Default 0 , name varchar(16) Not Null , store_size int Not Null Default 0 , is_deleted smallint Not Null Default 0 , turn_created_at int Not Null Default 0 , turn_deleted_at int Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN bots.id IS 'ボットID(シリアル値・自動カウント)' ; COMMENT ON COLUMN bots.user_id IS 'ユーザーID' ; COMMENT ON COLUMN bots.profession_id IS '職業ID' ; COMMENT ON COLUMN bots.strategy_id IS '戦略ID' ; COMMENT ON COLUMN bots.is_fighter IS '戦闘職(0: 非戦闘職, 1: 戦闘職)' ; COMMENT ON COLUMN bots.name IS 'ボット名' ; COMMENT ON COLUMN bots.store_size IS '最大アイテム所持数' ; COMMENT ON COLUMN bots.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN bots.turn_created_at IS '作成されたターン' ; COMMENT ON COLUMN bots.turn_deleted_at IS '削除されたターン(1以上: 削除されたターン, 0: 未削除)' ; COMMENT ON COLUMN bots.created_at IS '登録日時' ; COMMENT ON COLUMN bots.updated_at IS '更新日時' ; COMMENT ON TABLE bots IS 'ボット' ;
CREATE INDEX bots_user_id ON bots ( user_id ) ; CREATE INDEX bots_profession_id ON bots ( profession_id ) ;

Drop Table If Exists professions;
CREATE TABLE professions ( id serial primary key Not Null , name varchar(16) Not Null UNIQUE , max_level int Not Null Default 10 );
COMMENT ON COLUMN professions.id IS '職業ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN professions.name IS '職業名' ; COMMENT ON COLUMN professions.max_level IS '最大レベル' ; COMMENT ON TABLE professions IS '職業' ; 

Drop Table If Exists bot_professions;
CREATE TABLE bot_professions ( id serial primary key Not Null , bot_id int Not Null , profession_id int Not Null , level int Not Null Default 0 , is_deleted smallint Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN bot_professions.id IS 'ボットの職業ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN bot_professions.bot_id IS 'ボットID' ; COMMENT ON COLUMN bot_professions.profession_id IS '職業ID' ; COMMENT ON COLUMN bot_professions.level IS '職業レベル' ; COMMENT ON COLUMN bot_professions.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN bot_professions.created_at IS '登録日時' ; COMMENT ON COLUMN bot_professions.updated_at IS '更新日時' ; COMMENT ON TABLE bot_professions IS 'ボットの職業' ;
CREATE INDEX bot_professions_bot_id ON bot_professions ( bot_id ) ; CREATE INDEX bot_professions_profession_id ON bot_professions ( profession_id ) ; CREATE INDEX bot_professions_level ON bot_professions ( level ) ;

Drop Table If Exists actions;
CREATE TABLE actions ( id serial primary key Not Null , name varchar(16) Not Null UNIQUE );
COMMENT ON COLUMN actions.id IS '行動ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN actions.name IS '行動名' ; COMMENT ON TABLE actions IS '行動' ;

Drop Table If Exists profession_actions;
CREATE TABLE profession_actions ( id serial primary key Not Null , profession_id int Not Null , action_id int Not Null , cost int Not Null Default 0 );
COMMENT ON COLUMN profession_actions.id IS '職業の行動ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN profession_actions.profession_id IS '職業ID' ; COMMENT ON COLUMN profession_actions.action_id IS '行動ID' ; COMMENT ON COLUMN profession_actions.cost IS '実行コスト' ; COMMENT ON TABLE profession_actions IS '職業の行動' ;

Drop Table If Exists items;
CREATE TABLE items ( id serial primary key Not Null , item_kind_id int Not Null , name varchar(16) Not Null , rate int Not Null Default 0 );
COMMENT ON COLUMN items.id IS 'アイテムID(シリアル値・自動カウント)' ; COMMENT ON COLUMN items.item_kind_id IS 'アイテム種別ID' ; COMMENT ON COLUMN items.name IS 'アイテム名' ; COMMENT ON COLUMN items.rate IS '相場(0: 設定なし, 1以上: 商品の相場価格)' ; COMMENT ON TABLE items IS 'アイテム' ;
CREATE INDEX items_item_kind_id ON items ( item_kind_id ) ;

Drop Table If Exists item_kinds;
CREATE TABLE item_kinds ( id serial primary key Not Null , name varchar(16) Not Null );
COMMENT ON COLUMN item_kinds.id IS 'アイテム種別ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN item_kinds.name IS 'アイテム種別名' ; COMMENT ON TABLE item_kinds IS 'アイテム種別' ;

Drop Table If Exists bot_items;
CREATE TABLE bot_items ( id serial primary key Not Null , bot_id int Not Null , item_id int Not Null , price int Not Null Default 0 , is_deleted smallint Not Null Default 0 , turn_created_at int Not Null , turn_deleted_at int Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN bot_items.id IS 'ボットの所持品ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN bot_items.bot_id IS 'ボットID' ; COMMENT ON COLUMN bot_items.item_id IS 'アイテムID' ; COMMENT ON COLUMN bot_items.price IS '価格(0: 購入以外の入手, 1以上: 購入価格)' ; COMMENT ON COLUMN bot_items.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN bot_items.turn_created_at IS '作成されたターン' ; COMMENT ON COLUMN bot_items.turn_deleted_at IS '削除されたターン(1以上: 削除されたターン, 0: 未削除)' ; COMMENT ON COLUMN bot_items.created_at IS '登録日時' ; COMMENT ON COLUMN bot_items.updated_at IS '更新日時' ; COMMENT ON TABLE bot_items IS 'ボットの所持品' ;
CREATE INDEX bot_items_bot_id ON bot_items ( bot_id ) ; CREATE INDEX bot_items_item_id ON bot_items ( item_id ) ; CREATE INDEX bot_items_price ON bot_items ( price ) ;

Drop Table If Exists shops;
CREATE TABLE shops ( id serial primary key Not Null , bot_id int Not Null , name varchar(16) Not Null , rack_size int Not Null Default 0 , is_deleted smallint Not Null Default 0 , turn_created_at int Not Null , turn_deleted_at int Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN shops.id IS '店舗ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN shops.bot_id IS 'ボットID(店舗の持ち主)' ; COMMENT ON COLUMN shops.name IS '店舗名' ; COMMENT ON COLUMN shops.rack_size IS '棚の大きさ(陳列最大数)' ; COMMENT ON COLUMN shops.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN shops.turn_created_at IS '作成されたターン' ; COMMENT ON COLUMN shops.turn_deleted_at IS '削除されたターン(1以上: 削除されたターン, 0: 未削除)' ; COMMENT ON COLUMN shops.created_at IS '登録日時' ; COMMENT ON COLUMN shops.updated_at IS '更新日時' ; COMMENT ON TABLE shops IS '店舗' ;
CREATE INDEX shops_bot_id ON shops ( bot_id ) ;

Drop Table If Exists shop_items;
CREATE TABLE shop_items ( id serial primary key Not Null , shop_id int Not Null , item_id int Not Null , bot_id int Not Null Default 0 , price int Not Null Default 1 , is_deleted smallint Not Null Default 0 , is_traded smallint Not Null Default 0 , turn_created_at int Not Null , turn_deleted_at int Not Null Default 0 , turn_traded_at int Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN shop_items.id IS '店舗の商品ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN shop_items.shop_id IS '店舗ID' ; COMMENT ON COLUMN shop_items.item_id IS 'アイテムID' ; COMMENT ON COLUMN shop_items.bot_id IS '購入したボットID(0: 未取引, 1以上: 購入したボットのID)' ; COMMENT ON COLUMN shop_items.price IS '価格' ; COMMENT ON COLUMN shop_items.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN shop_items.is_traded IS '取引状況(1: 取引済み)' ; COMMENT ON COLUMN shop_items.turn_created_at IS '作成されたターン' ; COMMENT ON COLUMN shop_items.turn_deleted_at IS '削除されたターン(1以上: 削除されたターン, 0: 未削除)' ; COMMENT ON COLUMN shop_items.turn_traded_at IS '取引されたターン(1以上: 取引されたターン, 0: 未取引)' ; COMMENT ON COLUMN shop_items.created_at IS '登録日時' ; COMMENT ON COLUMN shop_items.updated_at IS '更新日時' ; COMMENT ON TABLE shop_items IS '店舗の商品' ;
CREATE INDEX shop_items_shop_id ON shop_items ( shop_id ) ; CREATE INDEX shop_items_item_id ON shop_items ( item_id ) ; CREATE INDEX shop_items_bot_id ON shop_items ( bot_id ) ; CREATE INDEX shop_items_price ON shop_items ( price ) ;

Drop Table If Exists strategies;
CREATE TABLE strategies ( id serial primary key Not Null , user_id int Not Null , name varchar(256) Not Null Default '' , create_user_id int Not Null , file text Not Null Default '' , access int Not Null Default 0 , is_deleted smallint Not Null Default 0 , created_at timestamp Not Null Default CURRENT_TIMESTAMP , updated_at timestamp Not Null Default CURRENT_TIMESTAMP );
COMMENT ON COLUMN strategies.id IS '戦略ID(シリアル値・自動カウント)' ; COMMENT ON COLUMN strategies.user_id IS 'ユーザーID' ; COMMENT ON COLUMN strategies.name IS '戦略名' ; COMMENT ON COLUMN strategies.create_user_id IS '作成者(ユーザーID)' ; COMMENT ON COLUMN strategies.file IS 'ファイル名(戦略の詳細が保存されているファイル)' ; COMMENT ON COLUMN strategies.access IS '公開範囲(0: private, 9: public)' ; COMMENT ON COLUMN strategies.is_deleted IS '削除状況(1: 削除済み)' ; COMMENT ON COLUMN strategies.created_at IS '登録日時' ; COMMENT ON COLUMN strategies.updated_at IS '更新日時' ; COMMENT ON TABLE strategies IS '戦略' ;
CREATE INDEX strategies_user_id ON strategies ( user_id ) ; CREATE INDEX strategies_create_user_id ON strategies ( create_user_id ) ;
