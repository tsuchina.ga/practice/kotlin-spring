# kotlin-spring

Kotlin + Spring Boot + Spring MVC + Spring Batch + JPA + PostgreSQL + ...

KotlinとSpringを利用してWebAPIとBatchを実装する練習

元々は一つのプロジェクトとして進めていたが、途中で飽きたので練習作として公開


## Botters

経済活動のあるファンタジー世界を夢想し、その世界に戦略通りに忠実に動くBot(ボット)を放つ。


## ディレクトリ

* main: Batch + WebAPI
* sql: PostgreSQLサーバ用ファイル
